package endpoints

import (
	"encoding/csv"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"albion-kills-api/internal/models"
)

type getBuildStats struct {
	db          *sqlx.DB
	itemNameMap map[string]string
}

type GetBuildStatsParams struct {
	Dataset string `path:"dataset"`
}

func (ep *getBuildStats) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB
	ep.itemNameMap = ctx.ItemNameMap

	ctx.Fizz.GET("/api/builds/:dataset", []fizz.OperationOption{
		fizz.Summary("Fetch build stats dataset"),
		fizz.Description("dataset may be one of: 1v1_high_ip_7_day, 1v1_high_elo_7_day"),
		fizz.Response("404", "Metric Not Found", nil, nil, nil),
		fizz.ID("get_build_stats"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getBuildStats) handle(c *gin.Context, params *GetBuildStatsParams) (*models.BuildStats, error) {
	nameParts := strings.Split(params.Dataset, ".")
	dataset := nameParts[0]
	fmt.Println("Dataset: ", dataset)

	stats, err := ep.fetchBuildStats(dataset)

	if err != nil {
		return nil, errors.Trace(err)
	}

	if len(stats) == 0 {
		return nil, errors.NotFoundf("Metric %s", dataset)
	}

	if len(nameParts) == 2 && nameParts[1] == "csv" {
		ep.renderCSV(c, stats)
		return nil, nil
	}

	r := &models.BuildStats{
		MetricName: params.Dataset,
		Builds:     stats,
	}

	return r, nil
}

func (ep *getBuildStats) fetchBuildStats(dataset string) ([]models.BuildStat, error) {
	query := `
select
    main_hand_item, off_hand_item, head_item, body_item, shoe_item, cape_item,
    usages,
    avg_item_power,
    kill_fame,
    death_fame,
    kills,
    deaths,
    assists,
    fame_ratio,
    win_rate
from build_stats where stat_name = :dataset
order by usages desc limit :build_limit;
    `

	rows, err := ep.db.NamedQuery(query, map[string]interface{}{
		"dataset":     dataset,
		"build_limit": buildStatLimit,
	})

	if err != nil {
		return nil, fmt.Errorf("Failed to execute main query: %v\n", err)
	}

	defer rows.Close()

	stats := make([]models.BuildStat, 0)

	for rows.Next() {
		stat := models.BuildStat{}

		err = rows.Scan(
			&stat.Build.MainHand.Item,
			&stat.Build.OffHand.Item,
			&stat.Build.Head.Item,
			&stat.Build.Body.Item,
			&stat.Build.Shoe.Item,
			&stat.Build.Cape.Item,
			&stat.Usages,
			&stat.AverageItemPower,
			&stat.KillFame,
			&stat.DeathFame,
			&stat.Kills,
			&stat.Deaths,
			&stat.Assists,
			&stat.FameRatio,
			&stat.WinRate,
		)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		decorateBuildWithItemNames(ep.itemNameMap, &stat.Build)

		stats = append(stats, stat)
	}

	return stats, nil
}

func (ep *getBuildStats) renderCSV(c *gin.Context, stats []models.BuildStat) {
	c.Writer.Header().Set("Content-Type", "text/csv")
	w := csv.NewWriter(c.Writer)

	w.Write([]string{
		"Main Hand ID",
		"Main Hand Name",
		"Off Hand ID",
		"Off Hand Name",
		"Head ID",
		"Head Name",
		"Body ID",
		"Body Name",
		"Shoe ID",
		"Shoe Name",
		"Cape ID",
		"Cape Name",
		"Usages",
		"Fame Ratio",
		"Win Rate",
		"Kills",
		"Deaths",
		"Assists",
		"Kill Fame",
		"Death Fame",
		"Average IP",
	})

	for _, stat := range stats {
		fameRatio := 0.0
		if stat.FameRatio != nil {
			fameRatio = *stat.FameRatio
		}

		w.Write([]string{
			stat.Build.MainHand.Item,
			stat.Build.MainHand.Name,
			stat.Build.OffHand.Item,
			stat.Build.OffHand.Name,
			stat.Build.Head.Item,
			stat.Build.Head.Name,
			stat.Build.Body.Item,
			stat.Build.Body.Name,
			stat.Build.Shoe.Item,
			stat.Build.Shoe.Name,
			stat.Build.Cape.Item,
			stat.Build.Cape.Name,
			fmt.Sprintf("%d", stat.Usages),
			fmt.Sprintf("%.2f", fameRatio),
			fmt.Sprintf("%.2f", stat.WinRate),
			fmt.Sprintf("%d", stat.Kills),
			fmt.Sprintf("%d", stat.Deaths),
			fmt.Sprintf("%d", stat.Assists),
			fmt.Sprintf("%d", stat.KillFame),
			fmt.Sprintf("%d", stat.DeathFame),
			fmt.Sprintf("%.2f", stat.AverageItemPower),
		})
	}

	w.Flush()
}
