package endpoints

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"albion-kills-api/internal/models"
)

type getPlayerWeaponStats struct {
	db          *sqlx.DB
	itemNameMap map[string]string
}

type GetPlayerWeaponStatsParams struct {
	Name         string `path:"name"`
	LookbackDays int    `query:"lookback_days" default:"30" validate:"omitempty,max=9999" description:"Length of lookback period for stats"`
}

func (ep *getPlayerWeaponStats) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB
	ep.itemNameMap = ctx.ItemNameMap

	ctx.Fizz.GET("/api/players/:name/stats/weapons", []fizz.OperationOption{
		fizz.Summary("Get player weapon stats"),
		fizz.ID("get_player_weapon_stats"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getPlayerWeaponStats) handle(c *gin.Context, params *GetPlayerWeaponStatsParams) (*models.PlayerWeaponStats, error) {
	stats, err := ep.fetchPlayerWeaponStats(params.Name, params.LookbackDays)

	if err != nil {
		return nil, errors.Trace(err)
	}

	r := &models.PlayerWeaponStats{
		PlayerName: params.Name,
		Weapons:    stats,
	}

	return r, nil
}

func (ep *getPlayerWeaponStats) fetchPlayerWeaponStats(name string, lookbackDays int) ([]models.WeaponStat, error) {
	query := `
select
	main_hand_item,
	count(*) as usages,
	avg(item_power) as avg_item_power,
	sum(kill_fame) as kill_fame,
	sum(death_fame) as death_fame,
	sum(is_kill) as kills,
	sum(is_death) as deaths,
	sum(is_assist) as assists,
	sum(kill_fame) / sum(death_fame) as fame_ratio,
	(sum(is_kill) + sum(is_assist)) / count(*) as win_rate
from
(
	select
		time, kl.main_hand_item, k.item_power, e.total_kill_fame as kill_fame, 0 as death_fame, is_primary as is_kill, 0 as is_death, not is_primary as is_assist
	from killers as k
	join events as e on e.event_id = k.event_id
	join loadouts as kl on kl.id = k.loadout
	WHERE k.name = :name
      and e.time > DATE_SUB(NOW(), INTERVAL :lookback_days DAY)
	UNION ALL
	select
		time, vl.main_hand_item, v.item_power, 0 as kill_fame, e.total_kill_fame as death_fame, 0 as is_kill, 1 as is_death, 0 as is_assist
	from victims as v
	join events as e on e.event_id = v.event_id
	join loadouts as vl on vl.id = v.loadout
	WHERE v.name = :name
      and e.time > DATE_SUB(NOW(), INTERVAL :lookback_days DAY)
    order by time desc
) as t
group by main_hand_item
order by usages desc;
    `

	rows, err := ep.db.NamedQuery(query, map[string]interface{}{
		"name":          name,
		"lookback_days": lookbackDays,
	})

	if err != nil {
		return nil, fmt.Errorf("Failed to execute main query: %v\n", err)
	}

	defer rows.Close()

	stats := make([]models.WeaponStat, 0)

	for rows.Next() {
		stat := models.WeaponStat{}

		err = rows.Scan(
			&stat.Weapon,
			&stat.Usages,
			&stat.AverageItemPower,
			&stat.KillFame,
			&stat.DeathFame,
			&stat.Kills,
			&stat.Deaths,
			&stat.Assists,
			&stat.FameRatio,
			&stat.WinRate,
		)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		stat.WeaponName = getGenericItemName(stat.Weapon, ep.itemNameMap)

		stats = append(stats, stat)
	}

	return stats, nil
}
