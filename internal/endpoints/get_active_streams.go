package endpoints

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"albion-kills-api/internal/models"
)

type getActiveStreams struct {
	db                   *sqlx.DB
	twitchClientId       string
	twitchClientSecret   string
	twitchToken          string
	twitchTokenFetchedOn time.Time
	cacheTTL             time.Duration
	cachedResponse       *ActiveStreamsResponse
	isUpdating           bool
	lastError            error
	sync.Mutex
}

type GetActiveStreamsParams struct {
	Skip int `query:"skip" default:"0" description:"Offset for paging data" validate:"min=0"`
	Take int `query:"take" default:"20" description:"Number of records to fetch" validate:"omitempty,max=20,min=0"`
}

type ActiveStreamsResponse struct {
	Streams    []models.Stream `json:"streams"`
	LastUpdate time.Time       `json:"last_update"`
}

func (ep *getActiveStreams) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB
	ep.twitchClientId = ctx.TwitchClientId
	ep.twitchClientSecret = ctx.TwitchClientSecret
	ep.cacheTTL = time.Minute * 5
	ep.cachedResponse = &ActiveStreamsResponse{Streams: make([]models.Stream, 0)}

	ctx.Fizz.GET("/api/streams", []fizz.OperationOption{
		fizz.Summary("List of online streams that are linked"),
		fizz.ID("get_active_streams"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getActiveStreams) handle(c *gin.Context, params *GetActiveStreamsParams) (*ActiveStreamsResponse, error) {
	if ep.needsUpdate() {
		go ep.update()
	}

	streams := ep.cachedResponse.Streams

	if len(streams) > 0 {
		start := minInt(params.Skip, len(streams))
		end := minInt(params.Skip+params.Take, len(streams))
		streams = streams[start:end]
	}

	response := &ActiveStreamsResponse{
		Streams:    streams,
		LastUpdate: ep.cachedResponse.LastUpdate,
	}

	return response, ep.lastError
}

func (ep *getActiveStreams) update() {
	ep.Lock()
	defer ep.Unlock()
	if ep.needsUpdate() {
		ep.isUpdating = true
		defer func() {
			ep.isUpdating = false
		}()
		streams, err := ep.fetchStreams()
		ep.lastError = err
		if err != nil {
			log.Println(err)
			sentry.CaptureException(fmt.Errorf("Failed to fetch active streams: %w", err))
			return
		}

		ep.cachedResponse.Streams = streams
		ep.cachedResponse.LastUpdate = time.Now()
		ep.lastError = nil
	}
}

func (ep *getActiveStreams) fetchStreams() ([]models.Stream, error) {
	log.Println("Started fetching live streams")
	twitchUsers, err := ep.fetchTwitchUsers()
	if err != nil {
		return nil, fmt.Errorf("Failed to fetch twitch users: %w", err)
	}

	streams, err := ep.fetchLiveTwitchStreamsForUsers(twitchUsers)
	if err != nil {
		return nil, fmt.Errorf("Failed to fetch live streams: %w", err)
	}

	log.Println("Finished fetching live streams")

	return streams, nil
}

func (ep *getActiveStreams) fetchTwitchUsers() ([]*models.TwitchUser, error) {
	query := `
    select SUBSTRING_INDEX(SUBSTRING_INDEX(group_concat(t.name order by w.last_update desc), ',', 1), ',', -1) as name, t.twitch_name, t.twitch_user_id
    from twitch_users as t
    left join player_fav_weapon as w on t.name = w.name
    where twitch_user_id != 0
    group by t.twitch_name, t.twitch_user_id;
    `

	rows, err := ep.db.Query(query)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]*models.TwitchUser, 0)

	for rows.Next() {
		user := &models.TwitchUser{}

		err = rows.Scan(&user.Name, &user.TwitchUsername, &user.TwitchUserId)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		users = append(users, user)
	}

	return users, nil
}

func (ep *getActiveStreams) fetchLiveTwitchStreamsForUsers(users []*models.TwitchUser) ([]models.Stream, error) {
	err := ep.tryUpdateToken()
	if err != nil {
		return nil, err
	}

	streams := make([]models.Stream, 0)

	userIds := make([]string, len(users))
	for i, user := range users {
		userIds[i] = fmt.Sprintf("user_id=%s", strconv.Itoa(user.TwitchUserId))
	}
	idx := 0

	for idx < len(userIds) {
		end := minInt(idx+99, len(userIds))
		userIdsChunk := userIds[idx:end]
		url := fmt.Sprintf("https://api.twitch.tv/helix/streams?first=100&%s&game_id=417528", strings.Join(userIdsChunk, "&"))
		request, err := http.NewRequest("GET", url, nil)
		if err != nil {
			return nil, err
		}
		request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", ep.twitchToken))
		request.Header.Set("Client-Id", ep.twitchClientId)
		resp, err := http.DefaultClient.Do(request)

		if err != nil {
			return nil, fmt.Errorf("Failed to fetch twitch streams: %v\n", err)
		}

		if resp.StatusCode >= 400 {
			return nil, fmt.Errorf("Got http status %d while fetching twitch user\n", resp.StatusCode)
		}

		responseBytes, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading live streams response: %v\n", err)
		}

		var streamsResponse *models.TwitchStreamsResponse
		err = json.Unmarshal(responseBytes, &streamsResponse)

		if err != nil {
			return nil, fmt.Errorf("Failed to parse twitch live streams response: %v\n", err)
		}

		for _, twitchStream := range streamsResponse.Data {
			var user *models.TwitchUser
			for _, twitchUser := range users {
				if strconv.Itoa(twitchUser.TwitchUserId) == twitchStream.UserID {
					user = twitchUser
				}
			}
			if user != nil {
				stream := models.Stream{
					AlbionName:   user.Name,
					StreamUrl:    fmt.Sprintf("https://twitch.tv/%s", twitchStream.UserLogin),
					TiwtchName:   twitchStream.UserName,
					Title:        twitchStream.Title,
					ViewerCount:  twitchStream.ViewerCount,
					StartedAt:    twitchStream.StartedAt,
					Language:     twitchStream.Language,
					ThumbnailURL: twitchStream.ThumbnailURL,
					Events:       make([]models.StreamEvent, 0),
				}
				err = ep.lookupLastEvents(&stream)
				if err != nil {
					log.Printf("Failed to lookup stream events: %v\n", err)
				}
				streams = append(streams, stream)
			}
		}
		idx = end
	}

	sort.Slice(streams, func(i, j int) bool {
		return streams[i].ViewerCount > streams[j].ViewerCount
	})

	return streams, nil
}

func (ep *getActiveStreams) lookupLastEvents(stream *models.Stream) error {
	query := `
(
	select v.event_id, UNIX_TIMESTAMP(e.time), l.main_hand_item, 0 as is_win
	from victims as v
	join events as e on e.event_id = v.event_id
	join loadouts as l on v.loadout = l.id
	where v.name = :name
        and e.time >= :streamStart
	order by event_id desc
)
UNION ALL
(
	select k.event_id, UNIX_TIMESTAMP(e.time), l.main_hand_item, 1 as is_win
	from killers as k
	join events as e on e.event_id = k.event_id
	join loadouts as l on k.loadout = l.id
	where k.name = :name
        and e.time >= :streamStart
	order by event_id
) order by event_id desc;
    `
	rows, err := ep.db.NamedQuery(query, map[string]interface{}{
		"name":        stream.AlbionName,
		"streamStart": stream.StartedAt,
	})

	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		event := models.StreamEvent{}
		rows.Scan(&event.Id, &event.Time, &event.Weapon, &event.IsKill)
		stream.Events = append(stream.Events, event)
	}

	return nil
}

func (ep *getActiveStreams) tryUpdateToken() error {
	if time.Now().Sub(ep.twitchTokenFetchedOn) > 24*time.Hour {
		return ep.updateToken()
	}

	return nil
}

func (ep *getActiveStreams) updateToken() error {
	url := fmt.Sprintf("https://id.twitch.tv/oauth2/token?client_id=%s&client_secret=%s&grant_type=client_credentials", ep.twitchClientId, ep.twitchClientSecret)
	fmt.Println("Getting twitch token:", url)
	resp, err := http.Post(url, "", nil)

	if err != nil {
		return fmt.Errorf("Failed to fetch twitch token: %v\n", err)
	}

	if resp.StatusCode >= 400 {
		return fmt.Errorf("Got http status %d while fetching twitch client token\n", resp.StatusCode)
	}

	responseBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return fmt.Errorf("Failed while reading twich token response: %v\n", err)
	}

	var token models.TwitchToken
	err = json.Unmarshal(responseBytes, &token)

	if err != nil {
		return fmt.Errorf("Failed to parse twitch token response: %v\n", err)
	}

	ep.twitchToken = token.AccessToken
	ep.twitchTokenFetchedOn = time.Now()

	return nil
}

func (ep *getActiveStreams) needsUpdate() bool {
	return ep.cachedResponse.LastUpdate.Before(time.Now().Add(-ep.cacheTTL)) && !ep.isUpdating
}

func minInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}
