package endpoints

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
)

type getPlayerWeaponMatrix struct {
	db          *sqlx.DB
	slayerCache *cachedMatrix
	cache       *cachedMatrix
}

type playerMatrixResponse struct {
	Matrix WeaponMatrix `json:"matrix"`
}

type GetPlayerWeaponMatrixParams struct {
	Name         string `path:"name"`
	LookbackDays int    `query:"lookback_days" default:"30" validate:"omitempty,max=9999" description:"Length of lookback period for stats"`
}

func (ep *getPlayerWeaponMatrix) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/players/:name/weapon-matrix", []fizz.OperationOption{
		fizz.Summary("Get player weapon matrix"),
		fizz.ID("get_player_weapon_matrix"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getPlayerWeaponMatrix) handle(c *gin.Context, params *GetPlayerWeaponMatrixParams) (*playerMatrixResponse, error) {
	matrix, err := ep.fetchWeaponMatrix(params.Name, params.LookbackDays)

	if err != nil {
		return nil, errors.Trace(err)
	}

	return &playerMatrixResponse{Matrix: matrix}, nil
}

func (ep *getPlayerWeaponMatrix) fetchWeaponMatrix(name string, lookbackDays int) (WeaponMatrix, error) {
	winsQuery := `
select my_weapon, their_weapon, count(*) as wins
from (
	select kl.main_hand_item as my_weapon, vl.main_hand_item as their_weapon
	from events as e
	join killers as k on e.event_id = k.event_id and k.is_primary = 1
	join victims as v on e.event_id = v.event_id
	join loadouts as kl on k.loadout = kl.id
	join loadouts as vl on v.loadout = vl.id
	WHERE e.participant_count = 1
	  and e.party_size = 1
	  and ((k.item_power > 900 and k.item_power < 1100 and v.item_power > 900 and v.item_power < 1100) OR (k.item_power > 1200 and v.item_power > 1200))
	  and kl.main_hand_item != "" and vl.main_hand_item != ""
      and k.name = ?
      and e.time > DATE_SUB(NOW(), INTERVAL ? DAY)
	order by e.event_id desc
) as t
group by my_weapon, their_weapon;
`

	lossesQuery := `
select my_weapon, their_weapon, count(*) as losses
from (
	select kl.main_hand_item as their_weapon, vl.main_hand_item as my_weapon
	from events as e
	join killers as k on e.event_id = k.event_id and k.is_primary = 1
	join victims as v on e.event_id = v.event_id
	join loadouts as kl on k.loadout = kl.id
	join loadouts as vl on v.loadout = vl.id
	WHERE e.participant_count = 1
	  and e.party_size = 1
	  and ((k.item_power > 900 and k.item_power < 1100 and v.item_power > 900 and v.item_power < 1100) OR (k.item_power > 1200 and v.item_power > 1200))
	  and kl.main_hand_item != "" and vl.main_hand_item != ""
      and v.name = ?
      and e.time > DATE_SUB(NOW(), INTERVAL ? DAY)
	order by e.event_id desc
) as t
group by my_weapon, their_weapon;
`

	winRows, err := ep.db.Query(winsQuery, name, lookbackDays)

	if err != nil {
		return nil, fmt.Errorf("Failed to run player wins weapon matrix query: %v\n", err)
	}

	defer winRows.Close()

	matrix := make(WeaponMatrix)

	for winRows.Next() {
		var myWeapon string
		var theirWeapon string
		var count int

		err = winRows.Scan(
			&myWeapon,
			&theirWeapon,
			&count,
		)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		if _, ok := matrix[myWeapon]; !ok {
			matrix[myWeapon] = make(map[string]*matrixDetail)
		}
		if _, ok := matrix[myWeapon][theirWeapon]; !ok {
			matrix[myWeapon][theirWeapon] = &matrixDetail{}
		}
		matrix[myWeapon][theirWeapon].Wins = count
	}

	lossRows, err := ep.db.Query(lossesQuery, name, lookbackDays)

	if err != nil {
		return nil, fmt.Errorf("Failed to run player losses weapon matrix query: %v\n", err)
	}

	defer lossRows.Close()

	for lossRows.Next() {
		var myWeapon string
		var theirWeapon string
		var count int

		err = lossRows.Scan(
			&myWeapon,
			&theirWeapon,
			&count,
		)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		if _, ok := matrix[myWeapon]; !ok {
			matrix[myWeapon] = make(map[string]*matrixDetail)
		}
		if _, ok := matrix[myWeapon][theirWeapon]; !ok {
			matrix[myWeapon][theirWeapon] = &matrixDetail{}
		}
		matrix[myWeapon][theirWeapon].Losses = count
	}

	return matrix, nil
}
