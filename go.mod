module albion-kills-api

go 1.15

require (
	github.com/Depado/ginprom v1.5.0
	github.com/JGLTechnologies/gin-rate-limit v1.5.4
	github.com/chai2010/webp v1.1.0
	github.com/getsentry/sentry-go v0.9.0
	github.com/gin-gonic/gin v1.9.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/websocket v1.5.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/juju/errors v0.0.0-20200330140219-3fe23663418f
	github.com/loopfz/gadgeto v0.11.0
	github.com/rs/cors v1.7.0
	github.com/segmentio/kafka-go v0.4.38
	github.com/wI2L/fizz v0.15.0
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
