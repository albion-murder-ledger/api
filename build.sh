#!/bin/bash

build_api() {
    go build -o api cmd/api/*.go
}

build_gateway() {
    go build -o gateway cmd/gateway/main.go
}

if [ "$1" == "api" ]; then
    build_api
elif [ "$1" == "gateway" ]; then
    build_gateway
else
    build_api
    build_gateway
fi
