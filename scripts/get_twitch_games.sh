#!/bin/bash
curl -X GET 'https://api.twitch.tv/helix/games?name=Albion%20Online' \
-H "Authorization: Bearer ${2}" \
-H "Client-Id: ${1}"
